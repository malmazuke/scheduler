A very simpler scheduler that was created for a Programming Assignment.

Takes a filename as a parameter, and reads in instructions from the filename - storing them in a Doubly Linked List.

For examples of input and output, look at the files ex.input & ex.output

~Mark Feaver
~2012
