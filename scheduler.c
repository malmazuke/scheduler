/**
  * A very simple scheduler program, that uses a Doubly
  * Linked List to store instructions that are read in
  * from a specified file.
  * Author: Mark Feaver
  * id: 1013657
  **/
  
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

//Current Scheduler time
uint16_t time_global;

//A node in the DLL
typedef struct node NODE_t;
struct node {
    char *instr;
    uint16_t time;
    uint16_t rep;
    NODE_t *next;
    NODE_t *prev;
};

/**
  *Creates a node
  **/
NODE_t *create_node(char *instr, uint16_t time, uint16_t rep){
    NODE_t *n = (NODE_t *) malloc(sizeof(NODE_t));
    n->instr = strdup(instr);
    n->time = time;
    n->rep = rep;
    n->next = NULL;
    n->prev = NULL;
    return n;
}

/**
  * Searches through the list for a given instruction
  **/
NODE_t *search(NODE_t *list, char *instr){
    if (list == NULL)
        return NULL;
    if (strcmp(list->instr, instr) == 0){
        return list;
    }
    
    return search(list->next, instr);
}
/**
  * Iteratively inserts a node into the list, starting from the tail
  **/
NODE_t *insert(NODE_t **head, NODE_t *tail, NODE_t *n){
    //If list is empty, insert at tail
    if (tail == NULL){
        *head = n;
        return n;
    }
    
    //If the time of the node is greater than the node
    //currently being looked at, insert it before it.
    if ((n->time) >= (tail->time)){
        n->prev = tail;
        if (tail->next != NULL)
            n->next = tail->next;
        tail->next = n;
        return n;
    }
    
    //Recurse
    tail->prev = insert(head, tail->prev, n);
    tail->prev->next = tail;
    return tail;
}

/**
  * Deletes the node where the instruction is found
  **/
NODE_t *delete(NODE_t *list, char *instr){
    //If not in list, return NULL
    if (list == NULL)
        return NULL;
    //If we have found a match, delete node and free memory
    if (strcmp(list->instr, instr) == 0){
        NODE_t *tmp = list->next;
        tmp->prev = list->prev;
        free(list->instr);
        free(list);
        return tmp;
    }
    //Recurse
    list->next = delete(list->next, instr);
    
    return list;
}

/**
  * Deletes the specified instruction + any repetitions
  **/
NODE_t *delete_reps(NODE_t *list, NODE_t **tail, char *instr){
    NODE_t *p = list;
    
    //Loop through entire list (this helps to find duplicates that
    //may have been created via ADD instead of ADDREP)
    while (p != NULL){
        //If match, delete the node and free memory
        if (strcmp((p->instr), instr) == 0){
            NODE_t *tmp = p->next;
            //In case we at are the tail
            if (tmp != NULL)
                tmp->prev = p->prev;
            else
                *tail = p->prev;
            p->prev->next = tmp;
            free(p->instr);
            free(p);
        }
        p = p->next;
    }
    return list;
}

/**
  * Prints out the entire list
  **/
void print_list(NODE_t *head){
    printf("Upcoming tasks:\n");
    NODE_t *p = head;
    while (p != NULL){
        uint16_t tdiff = p->time - time_global;
        printf("%s %u\n", p->instr, tdiff);
        p = p->next;
    }
}

/**
  * Executes the instructions before the current time
  **/
NODE_t *ex_instr(NODE_t *head, NODE_t **tail){
    //If head is empty, return NULL
    if (head == NULL)
        return head;
    //While the list is not empty and we are still within the available time,
    //check instructions for execution
    while ((head != NULL) && (head->time <= time_global)){
        NODE_t *tmp = head;
        //Handles repetitions
        if (tmp->rep > 0){
            NODE_t *n = create_node(tmp->instr, ((tmp->time)+(tmp->rep)), tmp->rep);
            *tail = insert(&head, *tail, n);
        }
        printf("%u %s\n", tmp->time, tmp->instr);
        head = delete(head, tmp->instr);
    }
    //If head is empty after deletion, we have a empty list
    if (head == NULL)
        *tail = NULL;
    return head;
}

/**
  * The main entry point for the program. Takes a filename as a 
  * parameter, and executes the instructions given in the file.
  * Prints to stdout
  **/
int main(int argc, char *argv[]){
    //---CLASS VARIABLES---//
    char line[1024];
    char *lsplt[4];
    char *token;
    FILE *fp;
    //The List
    NODE_t *tail = NULL;
    NODE_t *head = NULL;
    int count = 0;
    
    //Open the file for reading
    fp = fopen(argv[1], "r");
    
    //Handles errors in opening files
    if (fp == NULL){
        printf("usage: scheduler filename\n(Check that the filepath is also valid)\n");
        return -1;
    }
    
    //while there is still input
	while (fgets(line, sizeof(line), fp) != NULL){
	    //split the input
	    for (token = strtok(line, " "); token; token = strtok(NULL, " ")){
		    lsplt[count] = token;
		    count++;
	    }
	    
	    //String manipulation to remove the newline character
	    char *lstlne = lsplt[count-1];
	    lstlne[strlen(lstlne)-1] = '\0';
	    count = 0;
	    
	    //---HANDLING THE DIFFERENT CASES---//
	    //ADDING INSTRUCTIONS TO THE LIST
	    if (strcmp(lsplt[0], "ADD") == 0){
	        //Time
	        uint16_t tm = atoi(lsplt[1]);
	        //Only add to the list if it is greater than the current time
	        if (tm > time_global){
	            NODE_t *n = create_node(lsplt[2], tm, 0);
	            tail = insert(&head, tail, n);
	        }
	    }
	    
	    //REPETITIONS
	    else if (strcmp(lsplt[0], "ADDREP") == 0){
	    	uint16_t tm = atoi(lsplt[1]);
	        //Only add to the list if it is greater than the current time
	        if (tm > time_global){
	            NODE_t *n = create_node(lsplt[3], tm, atoi(lsplt[2]));
	            tail = insert(&head, tail, n);
	        }
	    }
	    
	    //SETTING TIME
	    else if (strcmp(lsplt[0], "TIME") == 0){
	        //Time
	        time_global = atoi(lsplt[1]);
	        head = ex_instr(head, &tail);
	    }
	    
	    //DISPLAY THE CURRENT INSTRUCTIONS/LIST CONTENTS
	    else if (strcmp(lsplt[0], "LIST") == 0){
	        print_list(head);
	    }
	    
	    //DELETE THE SPECIFIED INSTRUCTION
        else if (strcmp(lsplt[0], "DEL") == 0){
            head = delete_reps(head, &tail, lsplt[1]);
	    }
	    
	    //CLEAR THE ENTIRE LIST
	    else if (strcmp(lsplt[0], "CLEAR") == 0){
	        while (head != NULL){
                NODE_t *tmp = head->next;
                free(head->instr);
                free(head);
                head = tmp;
	        }
	        head = NULL;
	        tail = NULL;
	    }
	}
	//Close the reader
	fclose(fp);
}